<?php
/**
 * @file
 * easy_blog_post_by_month.tpl.php
 * theme archive blog
 */
?>
<div class="easy-blog-archive">
  <?php if (isset($archive) && is_array($archive)) : ?>
    <ul>
      <?php foreach ($archive as $year => $months): ?>
        <?php $month_first = reset($months); ?>
        <li>
          <?php if (!empty($months)) :?>
            <span class="btn-arrow"></span>
          <?php endif; ?>
          <a href="<?php print url('blog/all/' . $year); ?>" class="link-dropdown"><?php print $year; ?> <span class="number">(<?php print $month_first->year_count; ?>)</span></a>
          <ul>
            <?php foreach ($months as $month_name => $data): ?>
              <li>
                <?php if (isset($data->nodes) && is_array($data->nodes)) :?>
                  <span class="btn-arrow"></span>
                <?php endif; ?>
                <?php print l($month_name, 'blog/all/' . $data->year . '-' . $data->month_digits, array("attributes" => array("class" => array("link-dropdown")))); ?>
                <span class="number">(<?php print $data->count; ?>)</span>

                <?php if (isset($data->nodes) && is_array($data->nodes)): ?>
                  <ul>
                    <?php foreach ($data->nodes as $nid => $title): ?>
                      <li><?php print l($title, 'node/' . $nid); ?></li>
                    <?php endforeach; ?>
                  </ul>
                <?php endif; ?>
              </li>
            <?php endforeach; ?>
          </ul>
        </li>
      <?php endforeach; ?>
    </ul>
  <?php endif; ?>
</div>