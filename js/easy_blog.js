/**
 * @file
 * easy_blog.js
 * js behaviors for easy blog tree archive.
 */

(function($) {

    Drupal.behaviors.easyBlog = {
        attach: function(context, settings) {

            $('.easy-blog-archive .btn-arrow', context).click(function(e){
                $(this).toggleClass('active');
                $(e.target).nextAll('ul').toggleClass('active');
            });
        },

        completedCallback: function () {
            // Do nothing. But it's here in case other modules/themes want to override it.
        }

    }
})(jQuery);